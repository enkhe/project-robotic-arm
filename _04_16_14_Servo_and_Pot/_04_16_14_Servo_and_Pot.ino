#include <Servo.h>

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;


int servoPin1 = 3;
int servoPin2 = 4;
int servoPin3 = 5;
int servoPin4 = 6;
int servoPin5 = 7;

int potPin1 = A0;
int potPin2 = A1;
int potPin3 = A2;
int potPin4 = A3;
int potPin5 = A4;

int servoValue1 = 0;
int servoValue2 = 0;
int servoValue3 = 0;
int servoValue4 = 0;
int servoValue5 = 0;


void setup() {
  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo3.attach(servoPin3);
  servo4.attach(servoPin4);
  servo5.attach(servoPin5);
  
  Serial.begin(9600);
}

void loop() {
  
  doServoStuff(servo1, servoValue1, servoPin1);
  doServoStuff(servo2, servoValue2, servoPin2);
  doServoStuff(servo3, servoValue3, servoPin3);
  doServoStuff(servo4, servoValue4, servoPin4);
  doServoStuff(servo5, servoValue5, servoPin5);
  
}

void doServoStuff(Servo servo, int servoValue, int potPin) {
  
  servoValue = analogRead(potPin);
  servoValue = map(servoValue, 0, 1023, 0, 179);
  
  if(potPin == 4) {
    servoValue = servoValue / 6;
  }
  servo.write(servoValue);
}

