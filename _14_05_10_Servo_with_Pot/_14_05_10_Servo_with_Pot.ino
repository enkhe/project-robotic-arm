#include <Servo.h>

Servo myServo1;
Servo myServo2;
Servo myServo3;
Servo myServo4;
Servo myServo5;

int analogValue1 = 0;
int analogValue2 = 0;
int analogValue3 = 0;
int analogValue4 = 0;
int analogValue5 = 0;

int intServo1 = 3;
int intServo2 = 4;
int intServo3 = 5;
int intServo4 = 6;
int intServo5 = 7;

int intPotPin1 = A0;
int intPotPin2 = A1;
int intPotPin3 = A2;
int intPotPin4 = A3;
int intPotPin5 = A4;

void setup() {
  myServo1.attach(intServo1);
  myServo2.attach(intServo2);
  myServo3.attach(intServo3);
  myServo4.attach(intServo4);
  myServo5.attach(intServo5);
  Serial.begin(9600);
}

void loop() {
  doOne(intPotPin1, analogValue1, myServo1, false);
  doOne(intPotPin2, analogValue2, myServo2, false);
  doOne(intPotPin3, analogValue3, myServo3, false);
  doOne(intPotPin4, analogValue4, myServo4, false);
  doOne(intPotPin5, analogValue5, myServo5, false);
  //analogValue1 = analogRead(intPotPin1);
  //analogValue1 = map(analogValue1, 0, 1023, 0, 179);
  //myServo1.write(analogValue1);
  
}

void doOne(int potPin, int analogValue, Servo servo, boolean b) {
  analogValue = analogRead(potPin);
  analogValue = map(analogValue, 0, 1023, 0, 179);
  if(b == true) {
    analogValue = analogValue / 2;
  }
  servo.write(analogValue);
  delay(10);
}

