#include <Servo.h> 
const int row = 4;
const int col = 5;

int cD[row][col] = {
                {90, 60, 50, 160, 90},
                {90, 60, 50, 130, 90},
                {90, 60, 50, 140, 90},
                {90, 60, 50, 150, 90}
            };
            
            // first one is the grip
            
            // second one but goes down
            
            // third one turns down as number increase.
            
            //
            

Servo myservo1, myservo2, myservo3, myservo4, myservo5;

Servo servos[] = {myservo1, myservo2, myservo3, myservo4, myservo5};

int aVals[] = {0, 0, 0, 0, 0};

int sI[] = {7, 6, 5, 4, 3};

int pP[] = {A0, A1, A2, A3, A4};

boolean fTry = true;

void setup() 
{ 
  for(int i = 0; i < 5; i++) {
    servos[i].attach(sI[i]);
  }
  
  Serial.begin(9600);
} 


void loop() 
{
  if(fTry == true)
    readFromCoordMode();
  
  //readFromPotMode();
}

void readFromCoordMode() {
  
  for(int i = 0; i < row; i++) { // row
    for(int j = 0; j < col; j++) { // col
      
      //servos[j].write(cD[i][j]);
      
      
      Serial.print(cD[i][j]);
      
      if(j != col - 1) Serial.print(" | ");
      
    }
    Serial.println("");
    delay(1000);
  }
  fTry = false;
  Serial.println("Done with first batch of coordinates.");
}

void readFromPotMode() {
  for(int i = 0; i < 5; i++) {
    doOne(pP[i], aVals[i], servos[i], (i==4?true:false));
  }
}


void doOne(int potPin, int analogValue, Servo servo, boolean b) {
  analogValue = analogRead(potPin);
  analogValue = map(analogValue, 0, 1023, 0, 179);
  if(b == true) analogValue = analogValue / 2;
  servo.write(analogValue);
}


