#include <Servo.h> 


const int row = 10;
const int col = 5;

int cD[row][col] = {
  
            };
            
// false: Let's you control with potentiometers
// true: requires the coordinates for servos
boolean autoMode = true;
const int dMs = 20;

Servo myservo1, myservo2, myservo3, myservo4, myservo5;
Servo servos[] = {myservo1, myservo2, myservo3, myservo4, myservo5};

int sI[] = {3, 5, 6, 9, 11};
int pP[] = {A0, A1, A2, A3, A4};

void setup() 
{ 
  for(int i = 0; i < 5; i++) {
    servos[i].attach(sI[i]);
  }
  
  Serial.begin(9600);
} 

boolean fTry = true;

void loop() 
{
  if(autoMode && fTry) {
    readFromCoordMode();
  } else {
    readFromPotMode();
  }
}

void readFromCoordMode() {
  for(int i = 0; i < row; i++) {
    for(int j = 0; j < col; j++) {
      servos[j].write(cD[i][j]);
      Serial.print(cD[i][j]);
      if(j != col - 1) Serial.print(" | ");
    }
    Serial.println("");
    delay(dMs);
  }
  fTry = false;
  Serial.println("Done with batch of coordinates.");
}

void readFromPotMode() {
  Serial.print("{ ");
  for(int i = 0; i < col; i++) {
    Serial.print(moveServo(pP[i], servos[i]));
    if(i < col - 1) {
      Serial.print(", ");
    }
  }
  delay(dMs);
  Serial.println("}, ");
}

int moveServo(int potPin, Servo servo) {
  int aVal = map(analogRead(potPin), 0, 1023, 0, 179);
  servo.write(aVal);
  return aVal;
}
