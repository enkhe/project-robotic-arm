#include <Servo.h>

Servo servo1, servo2, servo3, servo4, servo5;
int servoValue1, servoValue2, servoValue3, servoValue4, servoValue5 = 0;

int servoPin1 = 3; // axis rotation
int servoPin2 = 4; // 
int servoPin3 = 5; // 
int servoPin4 = 6; // wrist
int servoPin5 = 7; // gripper

int potPin1 = A0;
int potPin2 = A1;
int potPin3 = A2;
int potPin4 = A3;
int potPin5 = A4;

int maxVal1 = 179;
int maxVal2 = 170;
int maxVal3 = 150;
int maxVal4 = 110;
int maxVal9 = 90;

int minVal1 = 10;
int minVal2 = 20;
int minVal3 = 50;

int angleHundred = 100;

int delayInMilliseconds = 200;

void setup() {
  doServoInitialization(servo1, minVal3);
  doServoInitialization(servo2, angleHundred);
  doServoInitialization(servo3, minVal3);
  doServoInitialization(servo4, minVal3);
  doServoInitialization(servo5, minVal3);
  
  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo3.attach(servoPin3);
  servo4.attach(servoPin4);
  servo5.attach(servoPin5);
  
  
  Serial.begin(9600);
}

void loop() {
  moveServoWithPotentiometer(servo1, servoValue1, potPin1, minVal3, maxVal4);
  moveServoWithPotentiometer(servo2, servoValue2, potPin2, minVal3, maxVal4);
  moveServoWithPotentiometer(servo3, servoValue3, potPin3, minVal3, maxVal4);
  moveServoWithPotentiometer(servo4, servoValue4, potPin4, minVal3, maxVal9);
  moveServoWithPotentiometer(servo5, servoValue5, potPin5, minVal3, maxVal4);
}

void moveServoWithPotentiometer(Servo servo, int servoValue, int potPin, int minAngle, int maxAngle) {
  servoValue = analogRead(potPin);
  servoValue = map(servoValue, 0, 1023, 0, 179);
  
  if(servoValue > minAngle && servoValue < maxAngle) {
    servo.write(servoValue);
    delay(delayInMilliseconds);
  }
}

void doServoInitialization(Servo servo, int minVal) {
    servo.write(minVal);
    delay(delayInMilliseconds);
}



