#include <Servo.h> 
 
Servo myservo1, 
      myservo2, 
      myservo3, 
      myservo4, 
      myservo5;

int analogValue1, 
    analogValue2, 
    analogValue3, 
    analogValue4, 
    analogValue5 = 0;
    
int intServo1 = 7;
int intServo2 = 6;
int intServo3 = 5;
int intServo4 = 4;
int intServo5 = 3;

int intPotPin1 = A0;
int intPotPin2 = A1;
int intPotPin3 = A2;
int intPotPin4 = A3;
int intPotPin5 = A4;


void setup() 
{ 
  myservo1.attach(intServo1);
  myservo2.attach(intServo2);
  myservo3.attach(intServo3);
  myservo4.attach(intServo4);
  myservo5.attach(intServo5);
  Serial.begin(9600);
} 

void loop() 
{
  doOne(intPotPin1, analogValue1, myservo1, false);
} 


void doOne(int potPin, int analogValue, Servo servo, boolean b) {
  analogValue = analogRead(potPin);
  analogValue = map(analogValue, 0, 1023, 0, 179);
  if(b == true) {
    analogValue = analogValue / 2;
  }
  servo.write(analogValue);
}


