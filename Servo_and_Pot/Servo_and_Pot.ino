#include <Servo.h> 
 
Servo myservo1;
Servo myservo2; 
int servoPos = 0; 
int intServo1 = 8;
int intServo2 = 9;
int intPotPin = 0;
int analogValue = 0;

void setup() 
{ 
  myservo1.attach(intServo1);
  myservo2.attach(intServo2);
  Serial.begin(9600);
} 

void loop() 
{ 
  analogValue = analogRead(intPotPin);
  int servoAngle = map(analogValue, 0, 1023, 0, 179);
  myservo1.write(servoAngle);
  myservo2.write(servoAngle);
  Serial.println(servoAngle);
  delay(20);
} 
void readInputWriteToSerial(int pinNum) {
  Serial.println(analogRead(pinNum));
}
