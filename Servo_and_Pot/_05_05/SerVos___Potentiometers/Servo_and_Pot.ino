#include <Servo.h> 
 
Servo myservo1;
Servo myservo2;

int intServo1 = 10;
int intServo2 = 9;

int intPotPin1 = A0;
int intPotPin2 = A1;

int analogValue1 = 0;
int analogValue2 = 0;

void setup() 
{ 
  myservo1.attach(intServo1);
  myservo2.attach(intServo2);

} 

void loop() 
{
  doServoStuff(intPotPin1, analogValue1, myservo1);
  doServoStuff(intPotPin2, analogValue2, myservo2);
  
} 

void doServoStuff(int potPin, int analogValue, Servo servo) {
  analogValue = analogRead(potPin);
  analogValue = map(analogValue, 0, 1023, 0, 179);
  servo.write(analogValue);
  delay(20);
}

