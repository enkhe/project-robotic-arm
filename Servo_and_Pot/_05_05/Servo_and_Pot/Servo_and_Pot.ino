#include <Servo.h> 
 
Servo myservo1;
Servo myservo2;
Servo myservo3;
Servo myservo4;
Servo myservo5;

int intServo1 = 10;
int intServo2 = 9;
int intServo3 = 8;
int intServo4 = 7;
int intServo5 = 6;

int intPotPin1 = A0;
int intPotPin2 = A1;
int intPotPin3 = A2;
int intPotPin4 = A3;
int intPotPin5 = A4;

int analogValue1 = 0;
int analogValue2 = 0;
int analogValue3 = 0;
int analogValue4 = 0;
int analogValue5 = 0;

void setup() 
{ 
  myservo1.attach(intServo1);
  myservo2.attach(intServo2);
  myservo3.attach(intServo3);
  myservo4.attach(intServo4);
  myservo5.attach(intServo5);
} 

void loop() 
{
  doServoStuff(intPotPin1, analogValue1, myservo1);
  doServoStuff(intPotPin2, analogValue2, myservo2);
  doServoStuff(intPotPin3, analogValue3, myservo3);
  doServoStuff(intPotPin4, analogValue4, myservo4);
  doServoStuff(intPotPin5, analogValue5, myservo5);
} 

void doServoStuff(int potPin, int analogValue, Servo servo) {
  analogValue = analogRead(potPin);
  analogValue = map(analogValue, 0, 1023, 0, 179);
  if(potPin == 7){
    analogValue = analogValue / 2;
  }
  servo.write(analogValue);
  delay(20);
}

